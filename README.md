This is our repository for the "Otaniemipeli" game.

Course: Studio 1: Mediaohjelmointi (CS-C2110) @ Aalto University

Group members: Minja Axelsson, Joonas Palosuo, Emilia Paltta and Juhana Saarinen

The game is a Scala Swing application.

Main class: uusipeli.Main.
